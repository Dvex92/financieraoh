import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule } from '@angular/router';

/* THIRD PARTY MODULE */
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';

/* COMPONENTS */
import { AppComponent } from './app.component';
import { HomeComponent } from './pages/home/home.component';
import { BalanceComponent } from './pages/balance/balance.component';

/* SERVICES */
import { UserService } from './services/user.service';
import { BalanceService } from './services/balance.service';
import { BalanceDetailComponent } from './pages/balance-detail/balance-detail.component';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    BalanceComponent,
    BalanceDetailComponent
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    FormsModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpModule,
    RouterModule,
  ],
  providers: [UserService, BalanceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
