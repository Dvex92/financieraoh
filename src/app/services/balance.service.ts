import { Injectable } from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/catch';
import 'rxjs/add/operator/map';
import * as xml2js from 'xml2js';

@Injectable()
export class BalanceService {

  private apiUrl = 'http://45.55.174.40/account.php';

  constructor(public http:Http) { }

  getBalance() {
    return this.http.get(this.apiUrl).map(res => {
      let resXML2JS;
      let cleanedString = res.text().replace(/<(\/?)([^:>\s]*:)?([^>]+)>/g, "<$1$3>");
      xml2js.parseString(cleanedString, function (err, result) {
        resXML2JS = result;
      });
      return resXML2JS;
    })
  }

}
