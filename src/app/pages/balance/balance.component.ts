import { Component, OnInit } from '@angular/core';

import { BalanceService } from '../../services/balance.service';

import { BalanceDetailComponent } from '../balance-detail/balance-detail.component';

@Component({
  selector: 'app-balance',
  templateUrl: './balance.component.html',
  styleUrls: ['./balance.component.css']
})
export class BalanceComponent implements OnInit {
  public soapRes;

  public amountDebt;
  public paymentMonth;
  public minimunPaymentMonth;
  public purchaseInstallment;
  public billingPeriod;
  public lastPayday;
  public commissions;

  public displayDetail: boolean = false;

  constructor(public balanceProvider: BalanceService) { }

  ngOnInit() {
  	this.balanceProvider.getBalance().subscribe(data => {
  		this.soapRes = data.Envelope.Body[0].getEECC_V2Response[0].getEECC_V2Result[0];
  		
  		this.amountDebt = this.soapRes.deuTot[0];
  		this.paymentMonth = this.soapRes.pagMes[0];
  		this.minimunPaymentMonth = this.soapRes.pagMinMes[0];
  		this.purchaseInstallment = this.soapRes.pagMinMes[0];
      this.billingPeriod = this.soapRes.perFac[0];
      this.lastPayday = this.soapRes.ultDiaPag[0];
      this.commissions = this.soapRes;
  	});
  }

  showDetails(){ this.displayDetail = true; }

  goBack(){ this.displayDetail = false; }

}
