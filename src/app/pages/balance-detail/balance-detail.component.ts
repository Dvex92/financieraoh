import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-balance-detail',
  templateUrl: './balance-detail.component.html',
  styleUrls: ['./balance-detail.component.css']
})
export class BalanceDetailComponent implements OnInit {

  @Input('soapres') soapRes;

  public details;
  public commision = {};
  public insurance = {};
  public pay = {};

  constructor() { }

  ngOnInit() {
  	console.log(this.soapRes);
  	this.details = this.soapRes.detalles[0].TP_DatPro_BE;

  	let index_comm, index_insur, index_pay;

  	this.details.forEach((detail, index) => {
  		let comm_str = 'COMISION';
  		let insu_str = 'DESGRAVAMEN';
  		let pay_str = 'PAGO-';

  		if( detail.desPro[0].includes(comm_str) ){
  			this.commision = detail;
  			index_comm = index;
  		}

  		if( detail.desPro[0].includes(insu_str) ){
  			this.insurance = detail;
  			index_insur = index;
  		}

  		if( detail.desPro[0].includes(pay_str) ){
  			this.pay = detail;
  			index_pay = index;
  		}
  	});

  	this.details.splice(index_comm, 1);
  	this.details.splice(index_insur, 1);
  	this.details.splice(index_pay, 1);
  }

}
