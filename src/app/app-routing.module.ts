import { NgModule }             from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard }            from './auth.guard';

/* PAGES */
import { HomeComponent }     from './pages/home/home.component';
import { BalanceComponent }  from './pages/balance/balance.component';

@NgModule({
  imports: [
    RouterModule.forRoot([
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full'
      },
      {
        path: 'home',
        component: HomeComponent,
      },
      {
        path: 'balance',
        component: BalanceComponent
      },
    ])
  ],
  exports: [RouterModule],
  providers: []
})
export class AppRoutingModule { }